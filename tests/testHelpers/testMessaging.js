import chai from 'chai';
import {TestSuite, confirmation} from './helper.js';

import {
  JOIN_ROOM,
  JOIN_ROOM_RESPONSE,
  SEND_MESSAGE,
  SEND_MESSAGE_LISTENER,
} from '../../app.js';

const {assert} = chai;

const testRoomCreation = new TestSuite('Messaging', function() {
  before((done) => {
    Promise.all([
      confirmation(this.testUser, JOIN_ROOM_RESPONSE),
      confirmation(this.testUser2, JOIN_ROOM_RESPONSE),
      confirmation(this.testUser3, JOIN_ROOM_RESPONSE),
    ]).then(() => done());

    this.testUser.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test1'});
    this.testUser2.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test2'});
    this.testUser3.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test3'});
  });

  it('should allow a client to send a message to the whole room', (done) => {
    Promise.all([
      confirmation(this.testUser2, SEND_MESSAGE_LISTENER),
      confirmation(this.testUser3, SEND_MESSAGE_LISTENER),
    ]).then((results) => {
      assert.equal(results[0].message, 'Hello World');
      assert.equal(results[0].userName, 'test1');
      assert.equal(results[1].message, 'Hello World');
      assert.equal(results[1].userName, 'test1');
      done();
    });
    this.testUser.emit(SEND_MESSAGE, {message: 'Hello World'});
  });
});

export default testRoomCreation;
