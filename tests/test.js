'use strict';

import Server from '../app.js';
import testRoomCreation from './testHelpers/testRoomCreation.js';
import testJoiningRoom from './testHelpers/testJoiningRoom.js';
import testLeavingRoom from './testHelpers/testLeavingRoom.js';
import testCommands from './testHelpers/testCommands.js';
import testMessaging from './testHelpers/testMessaging.js';

describe('Room Events', function() {
  testRoomCreation.run();
  testJoiningRoom.run();
  testLeavingRoom.run();
  testCommands.run();
  testMessaging.run();
});

// After all the tests, shutdown the server.
after(function() {
  Server.close();
});
