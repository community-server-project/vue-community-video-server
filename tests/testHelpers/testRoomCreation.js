import chai from 'chai';
import {TestSuite} from './helper.js';

import {
  JOIN_ROOM,
  JOIN_ROOM_RESPONSE,
  VD_PAUSED,
} from '../../app.js';

const {assert} = chai;

const testRoomCreation = new TestSuite('should allow a client to create room', function() {
  it('should allow a client to create room', (done) => {
    this.testUser.emit(JOIN_ROOM, {
      roomName: 'test',
      roomPass: 'password',
      userName: 'test',
    });
    this.testUser.on(JOIN_ROOM_RESPONSE, (data) => {
      assert.equal(data.roomName, 'test');
      assert.equal(data.roomPass, 'password');
      assert.equal(data.capacity, 1);
      assert.equal(data.admin, 'test');
      assert.equal(data.userNames.length, 1);
      assert.equal(data.userNames[0], 'test');
      assert.equal(data.videoID, 'dQw4w9WgXcQ');
      assert.equal(data.videoTime, 0);
      assert.equal(data.videoState, VD_PAUSED);
      done();
    });
  });

  it('should block a client from creating a room with no name', (done) => {
    this.testUser2.emit(JOIN_ROOM, {
      roomName: '',
      roomPass: 'password',
      userName: 'test',
    });
    this.testUser2.on(JOIN_ROOM_RESPONSE, (data) => {
      assert.equal(data.error, 'Room name can not be empty!');
      done();
    });
  });
});

export default testRoomCreation;
