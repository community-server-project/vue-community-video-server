/*
  The socket server used to host the video / chat rooms
 */
'use strict';

import express from 'express';
import http from 'http';
import cors from 'cors';
import SocketIO from 'socket.io';

const app = express();
app.use(cors());
/* eslint-disable */
const server = http.Server(app);
/* eslint-enable */
const io = new SocketIO(server);
const port = process.env.PORT || 5000;


// Constants
export const JOIN_ROOM = 'join room';
export const JOIN_ROOM_RESPONSE = 'room created';
export const JOIN_ROOM_LISTENER = 'someone joined';
export const LEAVE_ROOM = 'leave room';
export const LEAVE_ROOM_RESPONSE = 'leaving room';
export const LEAVE_ROOM_LISTENER = 'someone left';
export const CHANGE_VIDEO = 'change video';
export const CHANGE_VIDEO_LISTENER = 'listen for change video';
export const SEEK = 'seek in video';
export const SEEK_LISTENER = 'listen to seeking';
export const CHANGE_STATE = 'change video state';
export const CHANGE_STATE_LISTENER = 'change video state listener';
export const SEND_MESSAGE = 'send message';
export const SEND_MESSAGE_LISTENER = 'sender message listener';

export const VD_PLAYING = 'playing';
export const VD_PAUSED = 'paused';

// User meta data
/**
 * rooms is used to hold the meta data for rooms
 * @param {string} roomName - Name of the room
 * @param {string} roomPass - Password lock for the room
 * @param {integer} capacity - Number of people in the room
 * @param {string} admin - Username of the user who can make modifications to the room
 * @param {array} userNames - Names of the users that are part of the room
 * @param {boolean} restricted - If only admins can make modifications to the video (default: true)
 * @param {string} videoID - URL of the video being played
 * @param {string} videoState - State of the video (Paused, playing)
 * @param {integer} videoTime - Current time of the video
 *
 * Note: userNames does not assign a socket to a room, it just allows the server to keep in memory
 * the names of the sockets attached to a room.
 */
const rooms = {};
/**
 * meta data used to track a socket's current room allocation and user name.
 * @param {string} userName - Nickname they have chosen for themselves
 * @param {string} roomName - Name of the room they are a part of
 */
const users = {};

io.on('connection', onConnection);
io.origins('*:*');

/**
 * Once a connection is formed
 * @param {object} socket - The connection object that connects to the client.
 */
function onConnection(socket) {
  socket.emit('welcome', {message: 'Connected to socket server.'});

  /**
   * To join a room
   * @param {string} roomName - Name of the room
   * @param {string} roomPass - Password lock for the room
   * @param {string} userName - Username for the user
   */
  socket.on(JOIN_ROOM, function(data) {
    const {roomName, roomPass, userName} = data;

    if (!roomName) {
      // Empty room name
      socket.emit(JOIN_ROOM_RESPONSE, {error: 'Room name can not be empty!'});
      return;
    }

    if (!userName) {
      // Empty user name
      socket.emit(JOIN_ROOM_RESPONSE, {error: 'Username can not be empty!'});
      return;
    }

    if (rooms[roomName]) {
      // Room already exists
      const room = rooms[roomName];
      if (room.roomPass !== roomPass) {
        socket.emit(JOIN_ROOM_RESPONSE, {error: 'Incorrect password!'});
      } else {
        if (room.userNames.includes(userName)) {
          // Username is already taken
          socket.emit(JOIN_ROOM_RESPONSE, {error: 'Username is already taken!'});
        } else {
          // Add the user to the list
          room.capacity += 1;
          room.userNames.push(userName);
          users[socket.id] = {userName, roomName};
          socket.join(room.roomName);
          socket.emit(JOIN_ROOM_RESPONSE, room);
          socket.to(roomName).emit(JOIN_ROOM_LISTENER, {message: `${userName} has joined the room!`});
        }
      }
    } else {
      // Room does not exist
      const userNames = [userName];
      const data = {
        roomName,
        roomPass,
        capacity: 1,
        admin: userName,
        userNames,
        restricted: true,
        videoID: 'dQw4w9WgXcQ',
        videoState: VD_PAUSED,
        videoTime: 0,
      };
      rooms[roomName] = data;
      users[socket.id] = {userName, roomName};
      socket.join(roomName);
      socket.emit(JOIN_ROOM_RESPONSE, data);
      socket.to(roomName).emit(JOIN_ROOM_LISTENER, {message: `${userName} has joined the room!`});
    }
  });

  /**
   * Leave a room.
   */
  socket.on(LEAVE_ROOM, function() {
    if (users[socket.id]) {
      // That means they have joined a room and thus have meta data to delete
      const {userName, roomName} = users[socket.id];

      if (rooms[roomName]) {
        // Room exists, time to start leaving it. Remove the username from users metadata and update
        // room metadata.
        const room = rooms[roomName];

        room.capacity -= 1;
        room.userNames.splice(room.userNames.indexOf(userName), 1);

        socket.leave(roomName);
        delete users[socket.id];
        socket.emit(LEAVE_ROOM_RESPONSE, {message: 'Successfully left room!'});
        socket.to(roomName).emit(LEAVE_ROOM_LISTENER, {message: `${userName} has left the room!`});

        if (room.capacity === 0) {
          // room is empty. Delete it
          delete rooms[roomName];
        } else if (room.admin = userName) {
          const randomLeader = Math.random() * room.userNames.length;
          room.admin = room.userNames[randomLeader];
        }
      } else {
        // Room does not exist. From how we designed the system, this should not be called
        socket.emit(LEAVE_ROOM_RESPONSE, {error: 'Room does not exist!'});
      }
    }
  });

  /**
   * When the server is asked by a client to change the video URL
   * @param {string} videoURL - URL of the video
   */
  socket.on(CHANGE_VIDEO, function(data) {
    if (!users[socket.id]) return;

    const {videoURL} = data;
    const {userName, roomName} = users[socket.id];
    const regex = /https:\/\/www.youtube.com\/watch\?v=(.+)/gi;

    if (rooms[roomName].admin !== userName) {
      socket.emit(CHANGE_VIDEO_LISTENER, {error: 'You do not have permission to do this!'});
      return;
    }

    const regexResult = regex.exec(videoURL);

    if (!regexResult) {
      socket.emit(CHANGE_VIDEO_LISTENER, {error: 'Please provide a correct Youtube URL!'});
      return;
    }

    // Using the server 'io' to broadcast to everyone in the room.
    rooms[roomName].videoID = regexResult[1];
    io.to(roomName).emit(CHANGE_VIDEO_LISTENER, {videoID: regexResult[1]});
  });

  /**
   * When the server is asked by a client to seek to a particular point in the video
   * @param {string} seekTime - Current time of the video
   */
  socket.on(SEEK, function(data) {
    if (!users[socket.id]) return;

    const {seekTime} = data;
    const {userName, roomName} = users[socket.id];

    if (rooms[roomName].admin !== userName) {
      socket.emit(SEEK_LISTENER, {error: 'You do not have permission to do this!'});
      return;
    }

    // Using the server 'io' to broadcast to everyone in the room.
    rooms[roomName].videoTime = seekTime;
    io.to(roomName).emit(SEEK_LISTENER, {seekTime});
  });

  /**
   * Tell the whole server to change the video state to either playing to paused
   * @param {string} videoState - State of the video
   */
  socket.on(CHANGE_STATE, function(data) {
    if (!users[socket.id]) return;

    const {videoState} = data;
    const {userName, roomName} = users[socket.id];

    if (rooms[roomName].admin !== userName) {
      socket.emit(CHANGE_STATE_LISTENER, {error: 'You do not have permission to do this!'});
      return;
    }

    // Using the server 'io' to broadcast to everyone in the room.
    rooms[roomName].videoState = videoState;
    io.to(roomName).emit(CHANGE_STATE_LISTENER, {videoState});
  });

  /**
   * Send message to everyone in the room
   */
  socket.on(SEND_MESSAGE, function(data) {
    if (!users[socket.id]) return;

    const {message} = data;
    const {userName, roomName} = users[socket.id];

    io.to(roomName).emit(SEND_MESSAGE_LISTENER, {message, userName});
  });

  /**
   * User broke the socket connection. As such, we need to forcely remove them from any rooms.
   * Thankfully, we can just use a stripped down version of the leaving code
   */
  socket.on('disconnecting', function() {
    if (users[socket.id]) {
      // That means they have joined a room and thus have meta data to delete
      const {userName, roomName} = users[socket.id];
      delete users[socket.id];

      if (rooms[roomName]) {
        // Room exists, time to start leaving it. Remove the username from users metadata and update
        // room metadata.
        const room = rooms[roomName];

        room.capacity -= 1;
        room.userNames.splice(room.userNames.indexOf(userName), 1);
        socket.to(roomName).emit(LEAVE_ROOM_LISTENER, {message: `${userName} has left the room!`});

        if (room.capacity === 0) {
          // room is empty. Delete it
          delete rooms[roomName];
        }
      }
    }
  });
}

server.listen(port, () => console.log(`[INFO] Listening on port ${port}`));

export default server;
