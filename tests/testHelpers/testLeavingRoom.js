import chai from 'chai';
import {TestSuite, confirmation} from './helper.js';

import {
  JOIN_ROOM,
  JOIN_ROOM_RESPONSE,
  LEAVE_ROOM,
  LEAVE_ROOM_RESPONSE,
  LEAVE_ROOM_LISTENER,
} from '../../app.js';

const {assert} = chai;

const testLeavingRoom = new TestSuite('Leaving rooms', function() {
  before((done) => {
    Promise.all([
      confirmation(this.testUser, JOIN_ROOM_RESPONSE),
      confirmation(this.testUser2, JOIN_ROOM_RESPONSE),
      confirmation(this.testUser3, JOIN_ROOM_RESPONSE),
      confirmation(this.testUser4, JOIN_ROOM_RESPONSE),
    ]).then(() => done());

    this.testUser.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test1'});
    this.testUser2.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test2'});
    this.testUser3.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test3'});
    this.testUser4.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test4'});
  });

  it('should support a person leaving the room', (done) => {
    this.testUser4.emit(LEAVE_ROOM);
    this.testUser4.on(LEAVE_ROOM_RESPONSE, (data) => {
      assert(data.message, 'Successfully left room!');
      done();
    });
  });

  it('should notify other clients when someone leaves the room', (done) => {
    Promise.all([
      confirmation(this.testUser, LEAVE_ROOM_LISTENER),
      confirmation(this.testUser2, LEAVE_ROOM_LISTENER),
    ]).then((results) => {
      assert.equal(results[0].message, 'test3 has left the room!');
      assert.equal(results[1].message, 'test3 has left the room!');
      done();
    });
    this.testUser3.emit(LEAVE_ROOM);
  });

  it('should support administrator leaving the room', (done) => {
    this.testUser.on(LEAVE_ROOM_RESPONSE, (data) => {
      assert(data.message, 'Successfully left room!');

      this.testUser4.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test4'});
      this.testUser4.on(JOIN_ROOM_RESPONSE, (data) => {
        assert.equal(data.roomName, 'test');
        assert.equal(data.roomPass, 'password');
        assert.equal(data.capacity, 2);
        assert.notEqual(data.admin, 'test1');
        done();
      });
    });
    this.testUser.emit(LEAVE_ROOM);
  });

  it('should delete a room when every one leaves', (done) => {
    Promise.all([
      confirmation(this.testUser2, LEAVE_ROOM_RESPONSE),
      confirmation(this.testUser4, LEAVE_ROOM_RESPONSE),
    ]).then(() => {
      this.testUser.on(JOIN_ROOM_RESPONSE, (data) => {
        assert.equal(data.roomName, 'test');
        assert.equal(data.roomPass, 'changed password');
        assert.equal(data.capacity, 1);
        done();
      });
      this.testUser.emit(JOIN_ROOM, {
        roomName: 'test',
        roomPass: 'changed password',
        userName: 'test1',
      });
    });

    this.testUser2.emit(LEAVE_ROOM);
    this.testUser4.emit(LEAVE_ROOM);
  });
});

export default testLeavingRoom;
