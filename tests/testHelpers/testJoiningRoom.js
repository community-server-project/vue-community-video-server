import chai from 'chai';
import {TestSuite, confirmation} from './helper.js';

import {
  JOIN_ROOM,
  JOIN_ROOM_RESPONSE,
  JOIN_ROOM_LISTENER,
} from '../../app.js';

const {assert} = chai;

const testJoiningRoom = new TestSuite('Joining Rooms', function() {
  before((done) => {
    this.testUser.emit(JOIN_ROOM, {
      roomName: 'test',
      roomPass: 'password',
      userName: 'test',
    });
    this.testUser.on(JOIN_ROOM_RESPONSE, (data) => {
      done();
    });
  });

  it('should allow another client to join the room', (done) => {
    this.testUser2.on(JOIN_ROOM_RESPONSE, (data) => {
      assert.equal(data.roomName, 'test');
      assert.equal(data.roomPass, 'password');
      assert.equal(data.capacity, 2);
      assert.equal(data.admin, 'test');
      assert.equal(data.userNames.length, 2);
      assert.equal(data.userNames[1], 'test2');
      done();
    });
    this.testUser2.emit(JOIN_ROOM, {
      roomName: 'test',
      roomPass: 'password',
      userName: 'test2',
    });
  });

  it('should notify other clients when someone joins the room', (done) => {
    Promise.all([
      confirmation(this.testUser, JOIN_ROOM_LISTENER),
      confirmation(this.testUser2, JOIN_ROOM_LISTENER),
    ]).then((results) => {
      assert.equal(results[0].message, 'test3 has joined the room!');
      assert.equal(results[1].message, 'test3 has joined the room!');
      done();
    });
    this.testUser3.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test3'});
  });

  it('should block a client from joining with an incorrect password', (done) => {
    this.testUser3.emit(JOIN_ROOM, {
      roomName: 'test',
      roomPass: 'incorrect password',
      userName: 'just an user',
    });
    this.testUser3.on(JOIN_ROOM_RESPONSE, (data) => {
      assert.equal(data.error, 'Incorrect password!');
      done();
    });
  });

  it('should block a client from joining with taken username', (done) => {
    this.testUser4.emit(JOIN_ROOM, {
      roomName: 'test',
      roomPass: 'password',
      userName: 'test',
    });
    this.testUser4.on(JOIN_ROOM_RESPONSE, function(data) {
      assert.equal(data.error, 'Username is already taken!');
      done();
    });
  });

  it('should block a client from joining with empty username', (done) => {
    this.testUser5.emit(JOIN_ROOM, {
      roomName: 'test',
      roomPass: 'password',
      userName: '',
    });
    this.testUser5.on(JOIN_ROOM_RESPONSE, function(data) {
      assert.equal(data.error, 'Username can not be empty!');
      done();
    });
  });
});

export default testJoiningRoom;
