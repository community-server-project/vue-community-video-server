import chai from 'chai';
import {TestSuite, confirmation} from './helper.js';

import {
  JOIN_ROOM,
  JOIN_ROOM_RESPONSE,
  CHANGE_VIDEO,
  CHANGE_VIDEO_LISTENER,
  SEEK,
  SEEK_LISTENER,
  CHANGE_STATE,
  CHANGE_STATE_LISTENER,
  VD_PLAYING,
} from '../../app.js';

const {assert} = chai;

const testCommands = new TestSuite('Sending commands to a restricted server', function() {
  before((done) => {
    Promise.all([
      confirmation(this.testUser, JOIN_ROOM_RESPONSE),
      confirmation(this.testUser2, JOIN_ROOM_RESPONSE),
    ]).then(() => done());

    this.testUser.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test1'});
    this.testUser2.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test2'});
  });

  it('should allow admin to make changes to video URL', (done) => {
    Promise.all([
      confirmation(this.testUser, CHANGE_VIDEO_LISTENER),
      confirmation(this.testUser2, CHANGE_VIDEO_LISTENER),
    ]).then((results) => {
      assert(results[0].videoID, 'QNcxOXgNGJY');
      assert(results[1].videoID, 'QNcxOXgNGJY');
      done();
    });
    this.testUser.emit(CHANGE_VIDEO, {videoURL: 'https://www.youtube.com/watch?v=QNcxOXgNGJY'});
  });

  it('should reject non youtube links for protection', (done) => {
    this.testUser.on(CHANGE_VIDEO_LISTENER, function(data) {
      assert(data.error, 'Please provide a correct Youtube URL!');
      done();
    });
    this.testUser.emit(CHANGE_VIDEO, {videoURL: 'https://www.dodgylink.com/watch?v=QNcxOXgNGJY'});
  });

  it('should reject non admin wishes to make changes to video URL', (done) => {
    this.testUser2.on(CHANGE_VIDEO_LISTENER, (data) => {
      assert(data.error, 'You do not have permission to do this!');
      done();
    });
    this.testUser2.emit(CHANGE_VIDEO, {videoURL: 'https://www.youtube.com/watch?v=QNcxOXgNGJY'});
  });

  it('should allow admin to seek to a different part of the video', (done) => {
    const seekTime = 100;
    Promise.all([
      confirmation(this.testUser, SEEK_LISTENER),
      confirmation(this.testUser2, SEEK_LISTENER),
    ]).then((results) => {
      assert(results[0].seekTime, seekTime);
      assert(results[1].seekTime, seekTime);
      done();
    });
    this.testUser.emit(SEEK, {seekTime});
  });

  it('should reject non admin wishes to make changes to video URL', (done) => {
    this.testUser2.on(SEEK_LISTENER, function(data) {
      assert(data.error, 'You do not have permission to do this!');
      done();
    });
    this.testUser2.emit(SEEK, {seekTime: 100});
  });

  it('should allow admin to tell room to change video state (play)', (done) => {
    Promise.all([
      confirmation(this.testUser, CHANGE_STATE_LISTENER),
      confirmation(this.testUser2, CHANGE_STATE_LISTENER),
    ]).then((results) => {
      assert(results[0].videoState, VD_PLAYING);
      assert(results[1].videoState, VD_PLAYING);
      done();
    }).catch((err) => {
      console.log(err);
    });
    this.testUser.emit(CHANGE_STATE, {videoState: VD_PLAYING});
  });

  it('should reject non admin wishes to make changes to video state (play)', (done) => {
    this.testUser2.on(CHANGE_STATE_LISTENER, (data) => {
      assert(data.error, 'You do not have permission to do this!');
      done();
    });
    this.testUser2.emit(CHANGE_STATE, {videoState: VD_PLAYING});
  });

  it('should provide updated information when new user joins', (done) => {
    const seekTime = 100;
    // ----------- Video change listener -----------
    Promise.all([
      confirmation(this.testUser, CHANGE_VIDEO_LISTENER),
      confirmation(this.testUser2, CHANGE_VIDEO_LISTENER),
    ]).then((results) => {
      assert(results[0].videoID, 'QNcxOXgNGJY');
      assert(results[1].videoID, 'QNcxOXgNGJY');
      // ----------- State change listener -----------
      Promise.all([
        confirmation(this.testUser, CHANGE_STATE_LISTENER),
        confirmation(this.testUser2, CHANGE_STATE_LISTENER),
      ]).then((results) => {
        assert(results[0].videoState, VD_PLAYING);
        assert(results[1].videoState, VD_PLAYING);
        // ----------- Seek time listener -----------
        Promise.all([
          confirmation(this.testUser, SEEK_LISTENER),
          confirmation(this.testUser2, SEEK_LISTENER),
        ]).then((results) => {
          assert(results[0].seekTime, seekTime);
          assert(results[1].seekTime, seekTime);
          // Actual tests, to see if the room updates are remembered for new users
          this.testUser3.on(JOIN_ROOM_RESPONSE, function(data) {
            assert.equal(data.roomName, 'test');
            assert.equal(data.capacity, 3);
            assert.equal(data.roomPass, 'password');
            assert.equal(data.videoID, 'QNcxOXgNGJY');
            assert.equal(data.videoTime, seekTime);
            assert.equal(data.videoState, VD_PLAYING);
            done();
          });
          this.testUser3.emit(JOIN_ROOM, {roomName: 'test', roomPass: 'password', userName: 'test3'});
        });
        this.testUser.emit(SEEK, {seekTime});
      });
      this.testUser.emit(CHANGE_STATE, {videoState: VD_PLAYING});
    });
    this.testUser.emit(CHANGE_VIDEO, {videoURL: 'https://www.youtube.com/watch?v=QNcxOXgNGJY'});
  });
});

export default testCommands;
