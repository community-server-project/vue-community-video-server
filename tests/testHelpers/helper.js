import io from 'socket.io-client';

import {
  JOIN_ROOM,
  JOIN_ROOM_RESPONSE,
  JOIN_ROOM_LISTENER,
  LEAVE_ROOM,
  LEAVE_ROOM_RESPONSE,
  LEAVE_ROOM_LISTENER,
  CHANGE_VIDEO,
  CHANGE_VIDEO_LISTENER,
  SEEK,
  SEEK_LISTENER,
  CHANGE_STATE,
  CHANGE_STATE_LISTENER,
} from '../../app.js';

const ioOptions = {
  transports: ['websocket'],
  forceNew: true,
  reconnection: false,
};

/**
 * A class to hold a new test suite. Used to hold the values so you can break it down
 */
export class TestSuite {
  /**
   * Initialises the constructor
   * @param {string} name - Name of the test
   * @param {function} test - The function that contains the tests
   */
  constructor(name, test) {
    this.testUser = io('http://localhost:5000/', ioOptions);
    this.testUser2 = io('http://localhost:5000/', ioOptions);
    this.testUser3 = io('http://localhost:5000/', ioOptions);
    this.testUser4 = io('http://localhost:5000/', ioOptions);
    this.testUser5 = io('http://localhost:5000/', ioOptions);
    this.name = name;
    this.test = test;
  }

  /**
   * Run the tests
   */
  run() {
    describe(this.name, () => {
      after((done) => {
        this.testUser.disconnect();
        this.testUser2.disconnect();
        this.testUser3.disconnect();
        this.testUser4.disconnect();
        this.testUser5.disconnect();
        done();
      });
      afterEach((done) => {
        // Avoid doubling up on calls by removing all old event listeners in an 'it' block
        const events = [
          JOIN_ROOM, JOIN_ROOM_RESPONSE, JOIN_ROOM_LISTENER,
          LEAVE_ROOM, LEAVE_ROOM_RESPONSE, LEAVE_ROOM_LISTENER,
          CHANGE_VIDEO, CHANGE_VIDEO_LISTENER, SEEK, SEEK_LISTENER,
          CHANGE_STATE, CHANGE_STATE_LISTENER,
        ];
        const testUsers = [this.testUser, this.testUser2, this.testUser3, this.testUser4, this.testUser5];

        for (const test of testUsers) {
          for (const event of events) {
            test.off(event);
          }
        }

        done();
      });
      this.test.call(this);
    });
  }
}

/**
 * Helper function to wrap around a promise to accept
 * @param {socket} user - The socket object
 * @param {string} response - The response you are expecting
 * @return {promise} - The promise object that awaits for the socket to response
 */
export function confirmation(user, response) {
  return new Promise((resolve, reject) => user.on(response, (data) => resolve(data)));
}
